package nl.eleven.note.domain;

public enum Status {
    OPEN,
    GESTART,
    AFGEROND
}
