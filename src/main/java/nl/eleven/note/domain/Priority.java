package nl.eleven.note.domain;

public enum Priority {
    LAAG,
    MIDDEN,
    HOOG
}
