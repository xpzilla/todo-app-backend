package nl.eleven.note.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import nl.eleven.note.domain.Note;

public interface NoteRepository extends JpaRepository<Note , Long> {
}
