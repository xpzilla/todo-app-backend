package nl.eleven.note.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import nl.eleven.note.domain.Note;
import nl.eleven.note.repository.NoteRepository;

@RestController
@RequestMapping("note-backend/rest/v1/note")
public class NoteController {

    private NoteRepository noteRepository;

    @Autowired
    public NoteController(final NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @GetMapping
    public List<Note> getNotes() {
        return noteRepository.findAll();
    }

    @PostMapping("create")
    public Note createNote(@RequestBody final Note note) {
        return noteRepository.save(note);
    }

    @PostMapping("update")
    public Note updateNote(@RequestBody final Note changedNote) {
        final Optional<Note> optionalNote = noteRepository.findById(changedNote.getId());
        final Note toBeUpdateNote = optionalNote.orElseThrow(() -> new HttpServerErrorException(HttpStatus.BAD_REQUEST, ""));

        toBeUpdateNote.setDate(changedNote.getDate());
        toBeUpdateNote.setMessage(changedNote.getMessage());
        toBeUpdateNote.setPriority(changedNote.getPriority());
        toBeUpdateNote.setStatus(changedNote.getStatus());

        return noteRepository.save(toBeUpdateNote);
    }

    @DeleteMapping("delete/{id}")
    public void deleteNote(@PathVariable("id") long id) {
        noteRepository.deleteById(id);
    }
}
